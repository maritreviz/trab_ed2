/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * 
 * @author marit
 */
public class Gerar {
    Tweet tw[] = new Tweet[231];
    Metodos m = new Metodos();
    int i =0,j;
    Random gerador = new Random();
    Tweet[] va = new Tweet[5];
    
    
    public void importarArquivo(){																			//Importa o arquivo de texto do banco de dados
		try{
			Path caminhoLeitura = Paths.get("entrada1.txt");									//Caminho do arquivo de texto
			if(caminhoLeitura.toFile().exists()==true){														//O Arquivo s� pode ser lido se o caminho existir, caso contr�rio acarreta em um erro que � tratado pelo try e catch
				Charset utf8charset = Charset.forName("UTF-8");												//Tipo de codifica��o do Arquivo
				FileInputStream caminhoDoArquivo = new FileInputStream(caminhoLeitura.toFile().getPath());	//L� o arquivo de texto, � obrigat�rio passar um caminho/path no construtor, faz a leitura de dados bin�rios n�o importando a fonte
				InputStreamReader tradutorEncode = new InputStreamReader(caminhoDoArquivo, utf8charset); 	//Traduz os bytes com o encoding dado para o respectivo c�digo, em outras palavras � um decodificador para uma codifica��o espec�fica nesse caso "UTF-8" mas poderia ser "ISO-8859-1"
																											//L� bytes de um lado, converte em caracteres do outro, atrav�s do uso de uma codifica��o de caracteres (encoding)
				BufferedReader bufferLeitura = new BufferedReader(tradutorEncode);							//concatena os diversos chars para formar uma String atrav�s do m�todo readLine
				String linha = bufferLeitura.readLine();													//Passa o conte�do da primeira linha para uma vari�vel String
				while (linha != null) {
					
                                            String[] aCampos = linha.split("	");
                                                 Tweet t = new Tweet(Long.parseLong(aCampos[0]),Long.parseLong(aCampos[1]),aCampos[2],aCampos[3]);
                                                 tw[i] = t;
                                                // t.imprime();        
					i++;
					linha = bufferLeitura.readLine();
				}
                                
                                imprimeVetOrd();
				bufferLeitura.close();
				}
			else if(caminhoLeitura.toFile().exists()==false){												//Se o Arquivo de relat�rio n�o existe, para n�o dar problema ele ser� criado como arquivo vazio
				JOptionPane.showMessageDialog(null, "Banco de dados n�o lido, caminho/arquivo n�o encontrado");
			}
		}catch(Exception erro){		//Tratamento de erros
			erro.printStackTrace();
			System.out.println("Erro na leitura de arquivos");
		}
	}
    
    public void imprimeVetOrd(){
            for(int k = 0; k<229;k++){
               System.out.println("Posição " +k+ ":" +tw[k].getTweet_id());
            }
    }
    
    public void chamaquick(){
        //Tweet[] t = selectVetorAleatorio(tw,50);
        m.quickSort(tw,0,229,0,0);
    }
    public void chamaquickMediana(){
        m.quickSortMediana(tw,0,229,3);
    }
    public void chamaquickInseriton(){
        m.quickSortInsertion(tw, 0,229, 0, 0, 10);
    }
    /*public Tweet[] selectVetorAleatorio(Tweet[] t, int tam){
        Tweet[] va = new Tweet[tam];
        int k =0;
        while(k < tam){
            int p = gerador.nextInt(229);
            System.out.println("randomico:" + p);
            
            va[i] = t[p];
            k++;
        }
        for(int i = 0; i < tam; i ++){
            int p = gerador.nextInt(228);
            va[i].setTweet_id(t[p].getTweet_id());
            System.out.println("teste vetor novo" + va[i] + "---" + i);
        }
        return va;
    }
    public void impVet(){
        Tweet[] v = new Tweet[5];
        
        v = selectVetorAleatorio(tw,5);
    }*/
}