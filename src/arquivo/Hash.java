/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivo;

import java.util.logging.Logger;

/**
 *
 * @author Phlo
 */
public class Hash {

    private Tweet[] tw;
    private int tamanho;
    private int colisao = 0;

    public Hash(int tamanho) {
        this.tamanho = tamanho;
        tw = new Tweet[tamanho];
    }

    public boolean insereHash(Tweet tweet, long chave, int tipoColisao) {
        int i = 0;
        int pos = 0;
        while (i < tw.length) {
            pos = FuncaoHash(chave);
            if (tw[pos] == null) {
                tw[pos] = tweet;
                return true;
            } else {
                while (tw[pos] != null) {
                    switch (tipoColisao) {
                        case 0:
                            if (pos + hashEnderecamentoLinear(pos) >= tw.length) {
                                pos = -1;
                            }
                            pos = pos + hashEnderecamentoLinear(pos);

                            System.out.println(pos);
                            if (tw[pos] == null) {
                                tw[pos] = tweet;
                                return true;
                            }
                            colisao++;
                    
                        case 1:
                            return false;
                    }
                        
                }
                i++;

            }
        }
        return false;
    }

    public void imprime() {
        for (int i = 0; i < tamanho; i++) {
            System.out.println(+i + ":" + tw[i].getTweet_id());
        }
    }

    private int FuncaoHash(long chave) {

        return (int) (chave) % tamanho;
    }

    public int hashEnderecamentoLinear(int chave) {
        return 1;

    }


    public int getColisao() {
        return colisao;
    }

}
