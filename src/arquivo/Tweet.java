/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivo;
  
/**
 *
 * @author marit
 */
public class Tweet {
    private long user_id;
    private long tweet_id;
    private String tweet;
    private String date;

    public Tweet() {
    }

    
    public Tweet(long user_id, long tweet_id, String tweet, String date) {
        this.user_id = user_id;
        this.tweet_id = tweet_id;
        this.tweet = tweet;
        this.date = date;
    }
    public void imprime(){
        System.out.println(user_id + " " + tweet_id + " " + tweet + " " + date);
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public void setTweet_id(long tweet_id) {
        this.tweet_id = tweet_id;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getUser_id() {
        return user_id;
    }

    public long getTweet_id() {
        return tweet_id;
    }

    public String getTweet() {
        return tweet;
    }

    public String getDate() {
        return date;
    }
    
}
