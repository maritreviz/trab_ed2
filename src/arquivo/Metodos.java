/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivo;

import java.util.Random;

/**
 *
 * @author marit
 */
class Metodos {

    Random gerador = new Random();
    int comp = 0;
    int cop = 0;

    public void InsertionSort(Tweet[] vetor, int inicio, int fim) {
        long pivo;
        for (int i = inicio; i < fim; i++) {
            pivo = vetor[i].getTweet_id();
            for (int j = i - 1; j >= 0 && vetor[j].getTweet_id() > pivo; j--) {
                Tweet aux = vetor[j + 1];
                vetor[j + 1] = vetor[j];
                vetor[j] = aux;
            }
        }
    }

    public void HeapSort(Tweet[] vetor, int tamanho) {

        for (int i = tamanho / 2 - 1; i >= 0; i--) {
            heapify(vetor, tamanho, i);
        }

        for (int i = tamanho - 1; i >= 0; i--) {

            Tweet aux = vetor[0];
            vetor[0] = vetor[i];
            vetor[i] = aux;

            heapify(vetor, i, 0);
        }
    }

    void heapify(Tweet vetor[], int tamanho, int indice) {
        int maior = indice;
        int esq = 2 * indice + 1;
        int dir = 2 * indice + 2;

        if (esq < tamanho && vetor[esq].getTweet_id() > vetor[maior].getTweet_id()) {
            maior = esq;
        }
        if (dir < tamanho && vetor[dir].getTweet_id() > vetor[maior].getTweet_id()) {
            maior = dir;
        }

        if (maior != indice) {
            Tweet aux = vetor[indice];
            vetor[indice] = vetor[maior];
            vetor[maior] = aux;
            heapify(vetor, tamanho, maior);
        }
    }

    public int comparaTweet(long t1, long t2) {
        if (t1 == t2) {
            return 0;
        } else if (t1 < t2) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Swap -> M�todo usado para trocar elementos
     */
    private void swap(Tweet[] t, int indice1, int indice2) {
        Tweet tAux = t[indice1];
        t[indice1] = t[indice2];
        t[indice2] = tAux;
    }

    public int partition(Tweet[] tw, int inicio, int fim, int criterio, int ordem) {
        //If you want to implement randomized quick sort
        // just uncomment the next two lines of code
        //int p = inicio + (int)Math.random()*((fim-inicio)+1); 
        //swap(lista, inicio, p);
        int p = inicio;
        switch (criterio) {
            case 0:						//Ordenar por nome
                if (ordem == 0) {			//Ordem Crescente
                    for (int n = p + 1; n <= fim; n++) {
                        if (comparaTweet(tw[n].getTweet_id(), tw[p].getTweet_id()) < 0) {	//Palavra 1 < Palavra 2 
                            swap(tw, n, p + 1);
                            swap(tw, p, p + 1);
                            p++;
                            cop++;
                        }
                        comp++;
                    }

                } else if (ordem == 1) {	//Ordem Decrescente
                    for (int n = p + 1; n <= fim; n++) {
                        if (comparaTweet(tw[n].getTweet_id(), tw[p].getTweet_id()) > 0) {	//Palavra 1 > Palavra 2 
                            swap(tw, n, p + 1);
                            swap(tw, p, p + 1);
                            p++;
                            // cop++;
                        }
                    }
                    //comp++;
                }
                break;
        }
        return p;
    }

    public int partitionMediana(Tweet[] tw, int inicio, int fim, long mediana) {
        int i = inicio, j = fim;
        long tmp;
        long pivo = mediana;

        while (i <= j) {
            while (tw[i].getTweet_id() < pivo) {
                i++;
            }
            while (tw[j].getTweet_id() > pivo) {
                j--;
            }
            if (i <= j) {
                tmp = tw[i].getTweet_id();
                tw[i] = tw[j];
                tw[j].setTweet_id(tmp);
                i++;
                j--;
            }
        }
        return i;
    }

    public void quickSort(Tweet[] tw, int inicio, int fim, int criterio, int ordem) {
        if (inicio < fim) {
            int p = partition(tw, inicio, fim, criterio, ordem);
            quickSort(tw, inicio, p - 1, criterio, ordem);
            quickSort(tw, p + 1, fim, criterio, ordem);
        }
    }

    public int getComp() {
        return comp;
    }

    public int getCop() {
        return cop;
    }

    public void InsertionSortLong(long[] vetor, int tam) {
        long pivo;
        for (int i = 0; i < tam; i++) {
            pivo = vetor[i];
            for (int j = i - 1; j >= 0 && vetor[j] > pivo; j--) {
                long aux = vetor[j + 1];
                vetor[j + 1] = vetor[j];
                vetor[j] = aux;
            }
        }
    }

    public long mediana(Tweet[] tw, int fim, int inicio, int k) {
        int tam = fim - inicio + 1;
        long[] v = new long[k];
        int p;
        for (int i = 0; i < k; i++) {
            p = gerador.nextInt(tam);
            v[i] = tw[p].getTweet_id();
        }
        InsertionSortLong(v, k);
        if (k == 3) {
            return v[1];
        } else if (k == 5) {
            return v[2];
        }
        return -1;
    }

    public void manualSort(Tweet[] tw, int tam) {
        if (tam <= 1) {
            return;
        }
        if (tam == 2) {
            if (tw[0].getTweet_id() > tw[1].getTweet_id()) {
                swap(tw, 0, 1);
            }
        } else {
            if (tw[0].getTweet_id() > tw[1].getTweet_id()) {
                swap(tw, 0, 1);
            }
            if (tw[0].getTweet_id() > tw[2].getTweet_id()) {
                swap(tw, 0, 2);
            }
            if (tw[1].getTweet_id() > tw[2].getTweet_id()) {
                swap(tw, 1, 2);
            }
        }
    }

    public void quickSortMediana(Tweet[] tw, int inicio, int fim, int k) {
        int tam = inicio - fim + 1;
        if (tam < 3) {
            manualSort(tw, tam);
        } else {
            long mediana = mediana(tw, fim, inicio, k);
            int p = partitionMediana(tw, inicio, fim, mediana);
            quickSortMediana(tw, inicio, p - 1, k);
            quickSortMediana(tw, p + 1, fim, k);
        }
    }

    public void quickSortInsertion(Tweet[] tw, int inicio, int fim, int criterio, int ordem, int m) {
        int tam = inicio - fim + 1;
        if (tam <= m) {
            InsertionSort(tw, inicio, fim);
        } else {
            int p = partition(tw, inicio, fim, criterio, ordem);
            quickSortInsertion(tw, inicio, p - 1, criterio, ordem, m);
            quickSortInsertion(tw, p + 1, fim, criterio, ordem, m);
        }
    }

    public void shellSort(Tweet[] tw, int tam) {
        int pulo = 1;
        Tweet aux = new Tweet();

        while (pulo < tam) {
            pulo = pulo * 3 + 1;
        }

        pulo = pulo / 3;
        int c, j;

        while (pulo > 0) {
            for (int i = pulo; i < tam; i++) {
                aux = tw[i];
                j = i;
                while (j >= pulo && tw[j - pulo].getTweet_id() > aux.getTweet_id()) {
                    tw[j] = tw[j - pulo];
                    j = j - pulo;
                }
                tw[j] = aux;
            }
            pulo = pulo / 2;
        }
    }

    public void mergeSort(Tweet[] tw, int inicio, int fim) {
        //public void mergeSort(Tweet[] tw, int inicio, int fim) {
        if (tw != null && inicio < fim && inicio >= 0
                && fim < tw.length && tw.length != 0) {
            int meio = ((fim + inicio) / 2);
            mergeSort(tw, inicio, meio);
            mergeSort(tw, meio + 1, fim);
            //mergeSort(tw, inicio, meio);
            //mergeSort(tw, meio + 1, fim);
            merge(tw, inicio, meio, fim);
        }

    }

    public void merge(Tweet[] tw, int inicio, int meio, int fim) {
        Tweet[] auxiliar = new Tweet[tw.length];
        for (int i = inicio; i <= fim; i++) {
            auxiliar[i] = tw[i];
        }
        int comeco1 = inicio;
        int comeco2 = meio + 1;
        int i = inicio;
        while (comeco1 <= meio && comeco2 <= fim) {
            if (auxiliar[comeco1].getTweet_id() <= auxiliar[comeco2].getTweet_id()) {
                tw[i] = auxiliar[comeco1];
                comeco1++;
            } else {
                tw[i] = auxiliar[comeco2];
                comeco2++;
            }
            i++;
        }
        while (comeco1 <= meio) {
            tw[i] = auxiliar[comeco1];
            comeco1++;
            i++;
        }
        while (comeco2 <= fim) {
            tw[i] = auxiliar[comeco2];
            comeco2++;
            i++;
        }
    }

}
